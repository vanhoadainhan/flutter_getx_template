import 'package:flutter_getx_base/services/auth_service.dart';

class Repository {
  final AuthService authService;

  Repository({required this.authService});
}
