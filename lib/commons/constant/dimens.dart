const double kRadiusButton = 2.0;
const double kHeightLargeButton = 56.0;
const double kHeightDefaultButton = 38.0;
const double kPaddingHorizontal16 = 16.0;
