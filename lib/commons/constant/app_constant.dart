class AppConstant {
  AppConstant._();

  static const String token = 'token';
  static const String language = 'language';
  static const String savedAccountCode = 'savedAccountCode';
  static const String themeApp = 'themeApp';
}
